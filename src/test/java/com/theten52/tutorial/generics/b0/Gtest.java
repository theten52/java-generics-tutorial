package com.theten52.tutorial.generics.b0;

import org.junit.jupiter.api.Test;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * TODO
 *
 * @author wangjin
 * @date 2020/9/29
 */
public class Gtest {
    @Test
    public void test01() {
        // 对于继承的父类是泛型的情况
        ParameterizedType genericSuperclass = (ParameterizedType) Student.class.getGenericSuperclass();
        System.out.println("genericSuperclass:" + genericSuperclass);
        Type type = genericSuperclass.getActualTypeArguments()[0];
        System.out.println("type:" + type);
    }
}

class Person<T> {

}

class Student extends Person<String> {

}
