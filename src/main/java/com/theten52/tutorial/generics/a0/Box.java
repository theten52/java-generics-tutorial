package com.theten52.tutorial.generics.a0;

/**
 * 定义一个泛型类
 *
 * @author wangjin
 * @date 2020/10/2
 */
public class Box<T> {
    private T t;

    public void add(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }
}