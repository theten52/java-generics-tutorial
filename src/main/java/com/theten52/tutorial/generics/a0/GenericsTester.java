package com.theten52.tutorial.generics.a0;

/**
 * 泛型测试
 *
 * @author wangjin
 * @date 2020/10/2
 */
public class GenericsTester {
    public static void main(String[] args) {
        Box<Integer> integerBox = new Box<Integer>();
        Box<String> stringBox = new Box<String>();

        integerBox.add(10);
        stringBox.add(new String("Hello World"));

        System.out.printf("Integer Value :%d\n", integerBox.get());
        System.out.printf("String Value :%s\n", stringBox.get());

        //Integer Value :10
        //String Value :Hello World
    }
}

