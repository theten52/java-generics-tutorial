package com.theten52.tutorial.generics.c1;

import java.util.ArrayList;
import java.util.List;

public class GenericDemo {
    public static void main(String[] args) {
        List<Apple> apples = new ArrayList<Apple>();
        List<? extends Fruit> fruits = apples;
        //fruits.add(new Strawberry());//编译错误
        //fruits.add(new Fruit());//编译错误
    }
}
