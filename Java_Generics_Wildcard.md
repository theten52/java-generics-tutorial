[java泛型 通配符详解及实践](https://www.jianshu.com/p/e3d58360e51f "Java Generics Wildcard")
# java泛型 通配符详解及实践
对于泛型的原理和基础,可以参考笔者的上一篇文章:[java泛型,你想知道的一切](https://www.jianshu.com/p/cc793713a992 )。

## 一个问题代码
观察以下代码 :
```java
    public static void main(String[] args) {
        // 编译报错
        // required ArrayList<Integer>, found ArrayList<Number>
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Number> list2 = list1;

        // 可以正常通过编译,正常使用
        Integer[] arr1 = new Integer[]{1, 2};
        Number[] arr2 = arr1;
    }
```