[Java Generics Quick Tutorial](https://www.javacodegeeks.com/2011/04/java-generics-quick-tutorial.html "Java Generics Quick Tutorial")

[翻译文章](https://blog.csdn.net/dnc8371/article/details/106707772 "Java泛型快速教程")
## 泛型的动机
想要搞懂为什么会有泛型的最简单地方法就是考虑一种语法糖，它可能使您省去一些强制转换操作：
```java
List<Apple> box = ...;
Apple apple = box.get(0);
```
上面的代码是自说明的：box是对Apple类对象列表的引用。 get方法返回一个Apple实例，不需要强制转换。如果没有泛型，此代码将是：
```java
List box = ...;
Apple apple = (Apple) box.get(0);
```
不言自明，泛型的主要优点是让编译器跟踪**类型参数**（types parameters），执行类型检查和强制转换操作：编译器保证强制转换永远不会失败。

## 泛型工具

泛型工具引入了**类型变量**（type variable）的概念。根据Java语言规范，类型变量是由以下声明引入的无限制条件的标识符：
+ 泛型类声明
+ 泛型接口声明
+ 泛型方法声明
+ 泛型构造函数声明

## 泛型类和接口
如果类或接口具有一个或多个类型变量，则它是泛型的。类型变量由尖括号分隔，并遵循类（或接口）的命名规则：
```java
public interface List<T> extends Collection<T> {
    //do something...
}
```
粗略地说，类型变量充当泛型参数，并提供编译器进行检查所需的信息。

Java库中的许多类（例如整个Collections框架）都被修改为泛型的。 例如，我们在第一个代码段中使用的List接口现在是一个泛型类。 
在该代码段中，box是对List\<Apple>对象的引用，该对象是使用一个类型变量Apple实现了List接口的类的实例。 
类型变量是编译器在将get方法的结果自动转换为Apple引用时使用的参数。

实际上，新的泛型签名或接口List的get方法是：
```java
T get(int index);
```
方法get确实返回了一个T类型的对象，其中T是List\<T>声明中指定的类型变量。
## 泛型方法和构造函数
我们可以使用差不多的方式通过方法或构造函数声明一个或多个类型变量。
```java
 public static <t> T getFirst(List<T> list) 
```
此方法将接受对List\<T>的引用，并将返回类型T的对象。

### 例子
您可以在自己的类或泛型Java库类中利用泛型类。
#### 类型安全的写入...
例如，在下面的代码片段中，我们创建了一个实例List\<String>，其中填充了一些数据：
```java
List<String> str = new ArrayList<String>();
str.add("Hello ");
str.add("World.");
```
如果我们尝试将其他类型的对象放入List\<String>，则编译器将引发错误：
```java
str.add( 1 ); // 不能编译
```
#### …以及读取时
如果我们传递List \<String>引用，则始终保证可以从中检索String对象：
```java
String myString = str.get(0); 
```
#### 迭代
库中的许多类（例如Iterator\<T>）已得到增强并变得泛型化。 接口List\<T>的iterator()方法现在返回一个Iterator\<T>，我们可以轻松使用它，而无需转换通过其`T next()`方法返回的对象。
```java
for (Iterator<String> iter = str.iterator(); iter.hasNext();) {
    String s = iter.next();
    System.out.print(s);
}
```
#### 使用foreach
for每种语法都利用了泛型。先前的代码片段可以写成：
```java
for (String s: str) {
    System.out.print(s);
}
```
显然，这样更容易阅读和维护。
#### 自动装箱和自动拆箱
处理泛型时，将自动使用Java语言的自动装箱/自动拆箱功能，如以下代码片段所示：
```java
List<Integer> ints = new ArrayList<Integer>();
ints.add(0);
ints.add(1);
int sum = 0;
for (int i : ints) {
    sum += i;
}
```
但是请注意，装箱和拆箱会降低性能，因此，通常会出现warnings。
### 子类型（重要）
与其他面向对象的类型化语言一样，在Java中，可以构建类型的层次结构：
```
------Object
        丅
------Fruit---------------
        丅               丅
------Apple----          Strawberry
丅           丅
FujiApple   GreenApple
```

在Java中，类型T的子类型可以是扩展T的类型，也可以是直接或间接实现T的类型（如果T是接口）。 由于“成为...的子类型”是传递关系，因此，如果类型A是B的子类型，而B是C的子类型，则A也将是C的子类型。 在上图中：

+ FujiApple是Apple的子类型
+ Apple是Fruit的一种
+ FujiApple是Fruit的子类型。

每个Java类型也将是Object的子类型。

类型B的每个子类型A都可以分配给类型B的引用：
```java
Apple a = ...;
Fruit f = a;
```
### 泛型的子类型化
如果可以将Apple实例的引用分配给Fruit的引用，如上所示，那么List\<Apple>和List\<Fruit>之间是什么关系？ 
哪一个是另外一个的子类型？或者说，如果类型A是类型B的子类型，则C\<A>和C\<B>如何相互关联？

答案可能出乎意料是：它们之间没有任何关系（C\<A>和C\<B>之间）。 用更正式的词来说，泛型类型之间的子类型关系是不变的。

这意味着以下代码段非法的：
```java
List<Apple> apples = ...;
List<Fruit> fruits = apples;
```
以下内容也是如此：
```java
List<Apple> apples;
List<Fruit> fruits = ...;
apples = fruits;
```
但是为什么呢？一个苹果是一种水果，一盒苹果（一个List）也是一盒水果呀。

从某种意义上讲是这样，但是类型（类）封装了状态和操作。 如果一盒苹果是一盒水果会怎样？
```java
List<Apple> apples = ...;
List<Fruit> fruits = apples;
fruits.add(new Strawberry());
```
如果是这样，我们可以在列表中添加Fruit的其他不同子类型，但实际上我们必须禁止这样做。

相反，更直观：一盒水果不是一盒苹果，因为它可能是其他种类（子类型）水果（例如草莓）的盒子（列表）。

### 这样真地会有问题吗？

也许不会。Java开发人员感到惊讶的最强烈原因是数组的行为与泛型类型之间的不一致。泛型的子类型关系是不变的，而数组的子类型关系是协变的：如果类型A是类型B的子类型，则A[]是B[]的子类型：
````java
Apple[] apples = ...;
Fruit[] fruits = apples;
````
可是等等！如果我们重新使用上一节中出现的参数，最终可能会在一个Apple的array中添加Strawberry：
```java
Apple[] apples = new Apple[1];
Fruit[] fruits = apples;
fruits[0] = new Strawberry();
```
该代码确实可以编译，但是在运行时会以ArrayStoreException的形式引发错误。 由于数组的这种行为，在存储操作期间，Java运行时需要检查类型是否兼容。 显然，该检查还会增加您应该意识到的性能损失。

同样，泛型更安全地使用，并且可以“纠正”Java数组的这种类型的安全性弱点。

【注：解释了为什么当`Apple[] apples = new Apple[1];`时数组可以用`Fruit[] fruits = apples;`而当`List<Apple> apples = ...;`时泛型不可以用`List<Fruit> fruits = apples;`；
因为：泛型设计时考虑了更加安全的使用方式（算是原因之一吧）！】

在这种情况下，您现在想知道为什么数组的子类型关系是协变的，我将为您提供[Java Generics and Collections](https://www.amazon.com/Java-Generics-Collections-Maurice-Naftalin/dp/0596527756 "Java集合和泛型")给出的答案：
如果数组是不变的，则无法将引用传递给未知类型的对象数组到以下方法，例如：
```java
void sort(Object[] o);
```
因为数组是协变，所以以上方法无需每次都复制入参到Object[]，即可调用sort方法。

随着泛型的出现，数组的这种特性不再是必需的（我们将在本文的下一部分中看到），并且确实应该避免这种情况。

## 通配符（重要）

------

### 不变,协变,逆变的定义
引用自：[java泛型 通配符详解及实践](https://www.jianshu.com/p/e3d58360e51f )。

逆变与协变用来描述类型转换（type transformation）后的继承关系，其定义：如果A、B表示类型，f(⋅)表示类型转换，≤表示继承关系（比如，A≤B表示A是由B派生出来的子类）；
    
+ f(⋅)是逆变（contravariant）的，当A≤B时有f(B)≤f(A)成立；
+ f(⋅)是协变（covariant）的，当A≤B时有f(A)≤f(B)成立；
+ f(⋅)是不变（invariant）的，当A≤B时上述两个式子均不成立，即f(A)与f(B)相互之间没有继承关系。

在java泛型中,引入了 ?(通配符)符号来支持协变和逆变.

**通配符表示一种未知类型,并且对这种未知类型存在约束关系**

`? extends T`(**上边界通配符`upper bounded wildcard`**) 对应协变关系,表示 `?` 是继承自 `T`的任意子类型.**也表示一种约束关系,只能提供数据,不能接收数据.**

`?` 的默认实现是 `? extends Object`, 表示 `?` 是继承自Object的任意类型.

`? super T`(**下边界通配符`lower bounded wildcard`**) 对应逆变关系,表示 `?` 是 `T`的任意父类型.**也表示一种约束关系,只能接收数据,不能提供你数据.**

------

正如我们在本文前面的部分中所看到的，泛型类型的子类型关系是不变的。不过，有时我们还是希望以与普通类型相同地方式使用泛型：

+ Narrowing a reference (covariance)【缩小引用】
+ Widening a reference (contravariance)【放大引用】

### 缩小引用
例如，假设我们有一组盒子，每个盒子都有不同种类的水果。我们希望能够编写可以接受任何方法的方法。
或者是说，给定类型B的子类型A，我们想找到一种方法来使用类型C\<B>的引用（或方法参数），该引用可以接受C\<A>的实例。

为了完成此任务，我们可以使用带有`extends`的通配符，例如以下示例：
```java
List<Apple> apples = new ArrayList<Apple>();
List<? extends Fruit> fruits = apples;
```
`? extends`重新引入泛型类型的协变子类型：Apple是Fruit的子类型，而List\<Apple>是List\<? extends Fruit>。

### 放大引用

现在让我们介绍另一个通配符：`? super`。 给定类型A的超类型B，则C\<B>是C<? super A>的子类型：
```java
List<Fruit> fruits = new ArrayList<Fruit>();
List<? super Apple> = fruits;
```

### 如何使用通配符

我们主要要考虑作为方法参数时的情况，以及此参数在方法内如何使用这两个方面考虑。
现在已经有了足够的理论：我们如何利用这些新结构？

#### ? extends

让我们回到第二部分中介绍Java数组协变的示例：

```java
Apple[] apples = new Apple[1];
Fruit[] fruits = apples;
fruits[0] = new Strawberry(); 
```
如我们所见，当尝试通过对Fruit数组的引用将Strawberry添加到Apple数组时，此代码可以编译，但会导致运行时异常。

现在，我们可以使用通配符将此代码转换为与之对应的泛型代码：由于Apple是Fruit的子类型，因此我们将使用`? extends`通配符，以便能够将List\<Apple>的引用分配给List\<? extends Fruit>：

```java
List<Apple> apples = new ArrayList<Apple>();
List<? extends Fruit> fruits = apples;
fruits.add(new Strawberry()); //编译错误
```
然而，此时代码将无法编译！Java编译器现在阻止我们将Strawberry添加到Fruit列表中。我们将在编译时检测到错误，甚至不需要进行任何运行时检查（数组会在运行时检查这种情况），以确保将兼容类型添加到列表中。
即使我们尝试将Fruit实例添加到列表中，代码也不会编译通过：
```java
List<Apple> apples = new ArrayList<Apple>();
List<? extends Fruit> fruits = apples;
fruits.add(new Fruit()); //编译错误
```
实际上，您不能将任何东西放入其类型使用`? extends`通配符的结构中。

原因很简单： `? extends T`通配符告诉编译器我们正在处理类型T的子类型，但是我们不知道是哪一个。 由于没有办法推导出来，而且我们需要保证类型安全，因此不允许您在此类结构内放置任何内容。

【如何理解这一段话呢？我们假设我们把`List<? extends Fruit> fruits`作为方法A的参数，且此时我们在操作方法内的`fruits`变量。
我们调用方法A时可以将`List<Apple>`或者`List<Strawberry`当参数传入，故此时我们在方法A内部操作`fruits`变量时并不能确定它的类型参数到底是那一种。
因为不能确定`fruits`的类型变量，故此时我们不能使用`add()`方法添加任何类型的引用到`fruits`中。】

另一方面，由于我们知道它一定是T的子类型，因此我们可以从结构中获取数据，并保证它是T实例：
```java
Fruit get = fruits.get(0);
```
结论1:`? extends`操作时：可以获取具体带类型的数据，但是不能添加数据。赋值时：接受目标类型子类型的参数引用。

#### ? super

使用`? super `类型通配符的行为是什么？让我们从这个开始：
```java
List<Fruit> fruits = new ArrayList<Fruit>();
List<? super Apple> = fruits;
```
我们知道fruits是对Apple超类列表的一个引用。同样，我们不知道fruits的超类型是哪个，但是我们知道Apple及其任何子类型都将与其分配兼容。
确实，由于这种未知类型将同时是Apple和GreenApple超类型，因此我们可以这样写：
```java
fruits.add(new Apple());
fruits.add(new GreenApple());
```
如果我们尝试添加任何Apple超类型，编译器都会警告：
```java
fruits.add(new Fruit()); //编译错误
fruits.add(new Object()); //编译错误
```
由于我们不知道fruits的超类型是哪个，因此fruits不允许添加任何超类型实例。
【同样考虑`fruits`是方法参赛时的情况，调用方法时我们给`fruits`传入的实参的类型参赛是不确定的（只知道类型参赛是`Apple`的父类的一种，具体是哪个父类就无法确定了）。
因此我们操作`fruits`时可以添加`Apple`的子类实例引用，但是不不能添加`Apple`的父类实例引用。】

如何从这种类型的数据中获取数据呢？事实证明，您唯一可以获取到的是Object实例：由于我们无法知道它是哪个超类型，因此编译器只能保证它将是对Object的引用，因为Object是任何Java类型的超类型。

#### get和put(or add)原则或PECS规则

总结一下`? extends`和`? super`通配符的行为，我们得出以下结论：

+ 使用`? extends`通配符：如果需要从数据结构中获取对象。
+ 使用`? super`通配符：如果需要将对象放入数据结构。
+ 如果您需要同时做这两个事情，请不要使用任何通配符。

这就是Maurice Naftalin在他的[Java Generics and Collections](https://www.amazon.com/Java-Generics-Collections-Maurice-Naftalin/dp/0596527756 )中称为“**获取和放置原理(The Get and Put Principle)**”，在Joshua Bloch的[Effective Java](https://www.amazon.com/Effective-Java-2nd-Joshua-Bloch/dp/0321356683 )中称为PECS规则。

Bloch的助记符PECS来自“Producer Extends，Consumer Super”，可能更容易记住和使用。

## 方法签名中的通配符

如本系列第二部分中所见，在Java中（与许多其他类型化语言一样），Substitution原则是：可以将子类型分配给其任何超类型的引用。

这适用于分配任何引用的过程，即使将参数传递给函数或存储其结果也是如此。因此，该原理的优点之一是，在定义类层次结构时，可以编写“通用”方法来处理整个子层次结构，而与要处理特定对象实例的类无关。 到目前为止，在Fruit类的层次结构中，接受Fruit作为参数的函数将接受其任何子类型（例如Apple或Strawberry）。

从上一篇文章中可以看出，通配符可还原泛型类型的协变量和逆变量子类型：使用通配符，可以使开发人员编写可以利用到目前为止所展示的优点的函数。

例如，如果开发人员想要定义一个方法eat，该方法接受任何Fruits的列表，则可以使用以下签名：
```java
void eat(List<? extends Fruit> fruits);
```
由于Fruit类的任何子类型的列表都是`List<? extends Fruit>`，先前的方法将接受任何此类列表作为参数。请注意，如上一节所述，“获取和放置原则”（或PECS规则）将允许您从此类列表中检索对象并将其分配给Fruit引用。

另一方面，如果你想要要将实例添加到作为参数传递的fruits变量上（例如：调用`fruits.add(xxx)`方法），则应使用`? super`通配符：
```java
void store(List<? super Fruit> fruits);
```
这样，可以将任何水果超类的列表传递给`store`函数，并且可以安全地将任何水果子类型放入其中。

## 有界类型变量

事实上，泛型的灵活性比这更大。 类型变量可以有界，几乎与通配符有界不相上下（就像我们在第二部分中看到的一样）。 但是，类型变量不能以super为边界，而只能以extends为边界。 查看以下签名：
```java
public static <T extends I<T>> void name(Collection<T> t);
```
它接受类型受限制的对象的Collection：它必须满足`T extends I<T>`条件。起初，使用有界类型变量似乎并不比通配符更强大，但是稍后我们将详细介绍这些差异。

让我们假设层次结构中的一些（但不是全部）fruit（水果）可能是juicy（多汁）的，如下所示：
```java
public interface Juicy<T> {
    Juice<T> squeeze();
}
```
juicy(多汁)的fruit（水果）将实现此接口并发布squeeze(榨干)方法。

现在，您编写一个使用一堆水果并将其全部榨干的库方法。 您可以写的第一个签名可能是：
```java
<T> List<Juice<T>> squeeze(List<Juicy<T>> fruits);
```
使用有界类型变量，您将编写以下内容（实际上，它与以前的方法具有相同地擦除作用）：
```java
<T extends Juicy<T>> List<Juice<T>> squeeze(List<T> fruits);
```
到目前为止，一切都很好。但还不够，我们可以在相同位置中使用的相同参数，然后发现squeeze方法不起作用，例如，在以下情况下使用红色橘子列表：
```java
class Orange extends Fruit implements Juicy<Orange>;
class RedOrange extends Orange;
```
由于我们已经了解了PECS原理，因此我们将通过以下方式更改方法：
```java
<T extends Juicy<? super T>> List<Juice<? super T>> squeezeSuperExtends(List<? extends T> fruits);
```
此方法接受类型扩展自`Juicy<? super T>`的对象列表，换句话说，必须存在类型`S`，使得`T extends Juicy<S>`**和**`S super T`。

## 递归界限（Recursive Bounds）

也许`T extends Juicy<? super T>`会让您觉得放松。 这种界限称为递归界限，因为类型`T`必须满足的界限取决于`T`。
您可以在需要时使用递归界限，也可以将它们与其他种类的界限进行混合匹配。

因此，您可以编写具有以下界限的通用方法：

```java
<A extends B<A,C>, C extends D<T>>
```
请记住，这些示例仅用于说明泛型可以做什么。你将要使用的界限总是依赖于你放入类型层次结构中的约束。

## 使用多个类型变量

假设您想放宽在最后一个squeeze方法上设置的递归范围。让我们假设类型`T`可以扩展`Juicy<S>`，尽管`T`本身不扩展`S`。方法签名可以是：
```java
 <T extends Juicy<S>, S> List<Juice<S>> squeezeSuperExtendsWithFruit(List<? extends T> fruits); 
```
此签名与上一个签名相当（因为我们仅在方法参数中使用T），但有一个小优点：由于我们声明了通用类型`S`，因此该方法可以返回`List<Juice<S>`而不是` List<? super T>`，
在某些情况下很有用，因为编译器将根据您传递的方法参数帮助您确定`S`类型是哪种。由于要返回列表，因此您可能希望调用者能够从中获取某些信息，并且如上一部分所述，
您只能从列表中获取Object实例，例如`List<? super T>`。

如果需要，显然可以为`S`添加更多界限，例如：

```java
<T extends Juicy<S>, S extends Fruit> List<Juice<S>> squeezeSuperExtendsWithFruit(List<? extends T> fruits); 
```

## 多重界限（Multiple Bounds）

如果要对同一类型变量应用多个界限怎么办？事实上，您只能为每个泛型类型变量编写一个界限。因此，以下界限是非法的：

```java
 <T extends A, T extends B> // 非法 
```

编译器将失败，并显示以下消息：`T is already defined in…`。

必须使用不同的语法来表示多个界限，这是一个非常熟悉的表示法：

```java
 <T extends A & B> 
```

这意味着`T`扩展了`A`和`B`**两者**。请注意，[根据Java语言规范 第4.4章](http://java.sun.com/docs/books/jls/third_edition/html/typesValues.html#4.4 )的规定，界限的定义是：

+ 一个类型变量。
+ 一个类。
+ 一个接口类型，后面可以跟其他接口类型。

这意味着只能使用接口类型来表达多个界限。无法在多重绑定中使用类型变量，并且编译器将失败，并显示以下消息：`类型变量不能后面跟随其他界限。`

在我阅读的文档中，这个问题并不总是被表达的很清楚。

参考文献：

+ [The Gray Blog](https://thegreyblog.blogspot.com/ )的[JCG](https://www.javacodegeeks.com/p/jcg.html )合作伙伴Gray撰写的有关[Java泛型的系列](https://thegreyblog.blogspot.com/2011/03/java-generics-tutorial-part-i-basics.html )文章

编码愉快！ 不要忘记分享！

Byron

相关文章：

+ [Java Generics Examples](https://examples.javacodegeeks.com/java-basics/generics/java-generics-examples/ )
+ [Java Best Practices Series](https://www.javacodegeeks.com/?tag=java-best-practices )
+ [10 Tips for Proper Application Logging](https://www.javacodegeeks.com/2011/01/10-tips-proper-application-logging.html )
+ [Things Every Programmer Should Know](https://www.javacodegeeks.com/2010/12/things-every-programmer-should-know.html )
+ [9 Tips on Surviving the Wild West Development Process](https://www.javacodegeeks.com/2011/02/9-tips-on-surviving-wild-west.html )
+ [Laws of Software Design](https://www.javacodegeeks.com/2011/01/laws-of-software-design.html )
+ [Java Fork/Join for Parallel Programming](https://www.javacodegeeks.com/2011/02/java-forkjoin-parallel-programming.html )